stages:
  - test
  - push
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG

test:
  stage: test
  image: buildpack-deps:bookworm
  variables:
    DEBIAN_FRONTEND: noninteractive
    DJANGO_SETTINGS_MODULE: "castellum.settings.gitlab"
    POSTGRES_PASSWORD: "CHANGEME"
  services:
    - name: postgres:latest
      alias: db_default
    - name: postgis/postgis:latest
      alias: db_contacts
  before_script:
    - apt-get update -q && apt-get install -y -qq python3-pip python3-venv hunspell hunspell-de-de gdal-bin python3-psycopg2
    - python3 -m venv .venv --system-site-packages
    - .venv/bin/pip install -U pip setuptools wheel
    - .venv/bin/pip install -e .[test]
  script:
    - .venv/bin/ruff check castellum tests
    - .venv/bin/pospell -l de_DE -p .misc/spelling/de_DE.txt castellum/locale/de/LC_MESSAGES/django.po
    - .venv/bin/python -m django makemigrations --dry-run --check --noinput
    - .venv/bin/pytest --ds=$DJANGO_SETTINGS_MODULE
  coverage: '/TOTAL.*\s+(\d+\%)/'

test-py39:
  extends: test
  image: buildpack-deps:bullseye

docker:
  stage: push
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - >
      if [ "$CI_COMMIT_REF_NAME" = "$CI_DEFAULT_BRANCH" ]; then
        /kaniko/executor \
          --context "$CI_PROJECT_DIR" \
          --dockerfile "$CI_PROJECT_DIR/Dockerfile" \
          --destination "$CI_REGISTRY_IMAGE:latest" \
          --cache \
          --cache-repo "$CI_REGISTRY_IMAGE/cache"
      fi
    - >
      if [ -n "$CI_COMMIT_TAG" ]; then
        /kaniko/executor \
          --context "$CI_PROJECT_DIR" \
          --dockerfile "$CI_PROJECT_DIR/Dockerfile" \
          --destination "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" \
          --cache \
          --cache-repo "$CI_REGISTRY_IMAGE/cache"
      fi
    - >
      if [ -n "$CI_COMMIT_TAG" ] && ! echo "$CI_COMMIT_TAG" | grep -q "b"; then
        /kaniko/executor \
          --context "$CI_PROJECT_DIR" \
          --dockerfile "$CI_PROJECT_DIR/Dockerfile" \
          --destination "$CI_REGISTRY_IMAGE:stable" \
          --cache \
          --cache-repo "$CI_REGISTRY_IMAGE/cache"
      fi
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG

docs:test:
  stage: test
  before_script:
  - apk --no-cache add python3 py3-pip
  - python3 -m venv .venv
  - .venv/bin/pip install sphinx sphinx-rtd-theme
  script:
  - .venv/bin/sphinx-build -W -b html docs/usage/ docs_html

pages:
  stage: push
  before_script:
  - apk --no-cache add python3 py3-pip
  - python3 -m venv .venv
  - .venv/bin/pip install sphinx sphinx-rtd-theme
  script:
  - .venv/bin/sphinx-build -b html docs/usage/ public/docs/
  artifacts:
    paths:
    - public
    expire_in: 1 month
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

deploy:demo:
  image: buildpack-deps:bookworm
  stage: deploy
  script:
    - 'curl https://castellum-demo.mpib-berlin.mpg.de/d4f66ab1-75f6-48cc-8a67-c9b031330f22 -X POST -H "Authorization: Bearer $DEMO_TOKEN"'
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $DEMO_TOKEN
