Comparison to other Systems
===========================

Decentralized Excel Sheets
--------------------------

In many institutions, each research project manages its own participants in an
Excel sheet. While simple, this approach has significant drawbacks:

-   Difficult or impossible to implement GDPR rights such as access,
    correction, or deletion of personal data, especially after researchers
    have left the institution.
-   No enforcement of organizational rules and processes.

Sona Systems
------------

`Sona <https://www.sona-systems.com/>`__ is a participant recruitment system
for universities. It allows students to enroll in studies and tracks the
credits they receive if participation is part of their coursework.

-   Focus on universities: It tracks credits and assumes that subjects will
    participate without further activation.
-   Accessible by all participants, which significantly increases the risk of
    data leaks.

Prolific
--------

`Prolific <https://www.prolific.com/>`__ is a service with its own participant
pool, allowing you to submit online studies and receive pseudonymized results.

-   No need to implement GDPR rights because you only ever see pseudonymized
    data.
-   Only suitable for anonymous online studies.

Castellum
---------

Given these alternatives, Castellum is best suited for research institutions
that …

-   do on-site studies or studies that are not fully anonymous
-   do not have access to a large pool of students who are obligated to
    participate in studies
-   have high requirements on security and GDPR compliance
-   value open source and data autonomy
