Welcome to Castellum's documentation!
=====================================

.. toctree::
   :maxdepth: 2

   overview
   features
   roles
   legal-basis
   pseudonyms
   security
   comparison
   faqs

.. toctree::
   :maxdepth: 2
   :caption: Step-by-step guides

   guides/two-factor-authentication
   guides/subject-management
   guides/study-management
   guides/study-execution
   guides/pseudonyms
   guides/data-protection

   guides/consent-management
   guides/recruitment
   guides/appointments-external-scheduler
   guides/access-calendar-feeds

   guides/admin
