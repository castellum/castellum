Pseudonym management
====================

.. _subject-by-pseudonym:

Find subject by study pseudonym
-------------------------------

1.  Click on **Studies** on the front page
2.  In the list of studies, find the study and click **Execution**
3.  Go to the **By pseudonym** tab
4.  Enter the pseudonym. If there is more than one pseudonym list for this
    study, you also have to select the correct one.


.. _subject-by-general-pseudonym:

Find subject by pseudonym from a general pseudonym list
-------------------------------------------------------

1.  Click on **Subjects** on the front page
2.  Click on **Find by pseudonym** next to the search button. This button will
    only show up if you have access to a general pseudonym list.
3.  Select the correct pseudonym list. Enter the pseudonym.


.. _subject-get-pseudonym:

Get the pseudonyms of a subject
-------------------------------

1.  Click on **Studies** on the front page
2.  In the list of studies, find the study and click **Execution**
3.  In the list of participating subjects, click **Details**
4.  In the subject overview, the pseudonym is listed among the subject's
    contact data and operational hints

.. note::
    The pseudonyms are only shown once you click a button. Each access to a
    pseudonym is monitored to detect abuse.
