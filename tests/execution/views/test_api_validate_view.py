from model_bakery import baker

from castellum.pseudonyms.helpers import get_pseudonym
from castellum.pseudonyms.models import Domain
from castellum.recruitment.models import Participation


def test_apivalidateview(client, conductor, study, subject):
    domain = baker.make(Domain, context=study)
    baker.make(
        Participation, study=study, subject=subject, status=Participation.PARTICIPATING
    )
    pseudonym = get_pseudonym(subject, domain.key)

    url = f'/execution/api/studies/{study.pk}/domains/{domain.key}/{pseudonym}/'
    response = client.get(url, HTTP_AUTHORIZATION=f'token {conductor.token}')

    assert response.status_code == 200
    assert response.json() == {'pseudonym': pseudonym}


def test_normalize_pseudonym(client, conductor, study, subject):
    domain = baker.make(Domain, context=study)
    baker.make(
        Participation, study=study, subject=subject, status=Participation.PARTICIPATING
    )
    pseudonym = get_pseudonym(subject, domain.key)

    url = f'/execution/api/studies/{study.pk}/domains/{domain.key}/{pseudonym.lower()}/'
    response = client.get(url, HTTP_AUTHORIZATION=f'token {conductor.token}')

    assert response.status_code == 200
    assert response.json() == {'pseudonym': pseudonym}


def test_unrelated_domain(client, conductor, study, subject):
    domain = baker.make(Domain)
    baker.make(
        Participation, study=study, subject=subject, status=Participation.PARTICIPATING
    )
    pseudonym = get_pseudonym(subject, domain.key)

    url = f'/execution/api/studies/{study.pk}/domains/{domain.key}/{pseudonym}/'
    response = client.get(url, HTTP_AUTHORIZATION=f'token {conductor.token}')

    assert response.status_code == 404


def test_ignore_privacylevel(client, conductor, study, subject):
    subject.privacy_level = 2
    subject.save()

    domain = baker.make(Domain, context=study)
    baker.make(
        Participation, study=study, subject=subject, status=Participation.PARTICIPATING
    )
    pseudonym = get_pseudonym(subject, domain.key)

    url = f'/execution/api/studies/{study.pk}/domains/{domain.key}/{pseudonym}/'
    response = client.get(url, HTTP_AUTHORIZATION=f'token {conductor.token}')

    assert response.status_code == 200
    assert response.json() == {'pseudonym': pseudonym}
