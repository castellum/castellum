import pytest
from django.conf import settings
from django.contrib.messages import get_messages
from model_bakery import baker

from castellum.contacts.models import Contact
from castellum.recruitment.models import Participation
from castellum.recruitment.models import SubjectFilter
from castellum.recruitment.models import SubjectFilterGroup
from castellum.subjects.models import Consent


def create_subject():
    contact = baker.make(Contact)
    subject = contact.subject
    subject.attributes = {'d1': 1}
    subject.save()
    baker.make(Consent, subject=subject, _fill_optional=['document'])
    return subject


def create_participation(study, status=Participation.NOT_CONTACTED):
    subject = create_subject()
    baker.make(Participation, subject=subject, study=study, status=status)


def test_min_subject_count_is_zero(client, member, study):
    study.min_subject_count = 0
    study.save()

    client.force_login(member)
    url = f'/recruitment/{study.pk}/'
    response = client.post(url)

    messages = list(get_messages(response.wsgi_request))
    assert len(messages) == 1
    assert str(messages[0]) == (
        'This study does not require participants. '
        'Please, contact the responsible person who can set it up.'
    )
    assert response.status_code == 200


@pytest.mark.parametrize('subject_count', [
    10 * settings.CASTELLUM_RECRUITMENT_HARD_LIMIT_FACTOR,
    11 * settings.CASTELLUM_RECRUITMENT_HARD_LIMIT_FACTOR,
])
def test_not_contacted_count_is_gte_to_hard_limit(client, member, study, subject_count):
    study.min_subject_count = 10
    study.save()

    for _i in range(subject_count):
        create_participation(study)

    client.force_login(member)
    url = f'/recruitment/{study.pk}/'
    response = client.post(url)

    messages = list(get_messages(response.wsgi_request))
    assert len(messages) == 1
    assert str(messages[0]) == (
        'Application privacy does not allow you to add more subjects. '
        'Please contact provided subjects before adding new ones.'
    )
    assert response.status_code == 200


def test_no_matching_subjects(client, member, study, attribute):
    study.min_subject_count = 5
    study.save()

    for _i in range(5):
        create_subject()

    subject_filter_group = baker.make(SubjectFilterGroup, study=study)
    baker.make(
        SubjectFilter,
        group=subject_filter_group,
        attribute=attribute,
        operator='exact',
        value='foo',
    )

    client.force_login(member)
    url = f'/recruitment/{study.pk}/'
    response = client.post(url)

    messages = list(get_messages(response.wsgi_request))
    assert len(messages) == 1
    assert str(messages[0]) == (
        'No potential participants could be found for this study.'
    )
    assert response.status_code == 200
