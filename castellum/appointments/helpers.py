# SPDX-FileCopyrightText: 2018-2025 MPIB <https://www.mpib-berlin.mpg.de/>
# SPDX-FileCopyrightText: 2018-2019 MPI-CBS <https://www.cbs.mpg.de/>
# SPDX-FileCopyrightText: 2018-2019 MPIP <http://www.psych.mpg.de/>
# SPDX-License-Identifier: AGPL-3.0-or-later

import logging
from urllib.parse import quote_plus
from urllib.parse import urljoin

import requests
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.urls import reverse
from django.utils import formats
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from icalevents import icalparser

from castellum.pseudonyms.helpers import get_pseudonym
from castellum.recruitment.models import Participation
from castellum.utils import cached_request
from castellum.utils import contrast_color
from castellum.utils import scheduler
from castellum.utils.mail import MailContext

from .models import Appointment

logger = logging.getLogger(__name__)


def date_format(dt):
    ldt = timezone.localtime(dt)
    return formats.date_format(ldt, 'l, ') + formats.date_format(ldt, 'DATETIME_FORMAT')


def get_reminder_body(name, session, dt):
    def get_mail_data():
        return {
            'name': name,
            'study': session.study.name,
            'date': date_format(dt),
            'reply_to': session.study.email,
            'session_reminder_text': session.reminder_text.strip() or '—',
        }

    return (
        get_mail_data,
        settings.CASTELLUM_APPOINTMENT_MAIL_BODY,
        settings.CASTELLUM_APPOINTMENT_MAIL_BODY_EN,
    )


def send_appointment_notifications(participation, change, base_url, user=None):
    old, new = change

    def format_state(state):
        if not state:
            return _('not scheduled')
        dt = formats.date_format(timezone.localtime(state.start), 'DATETIME_FORMAT')
        assigned = ', '.join(
            str(user) for user in state.assigned_conductors
        ) or _('not assigned')
        return f'{dt} ({assigned})'

    def get_mail_data():
        return {
            'study': participation.study,
            'change': f'{format_state(old)} -> {format_state(new)}',
            'participation_url': urljoin(base_url, reverse(
                'execution:participation-detail',
                args=[participation.study.pk, participation.pk],
            )),
            'calendar_url': urljoin(base_url, reverse(
                'execution:calendar',
                args=[participation.study.pk],
            )),
        }

    if not old and new and new.assigned_conductors:  # create
        conductors = new.assigned_conductors
    elif not new and old and old.assigned_conductors:  # delete
        conductors = old.assigned_conductors
    elif new and old and new.assigned_conductors and old.assigned_conductors:  # change
        conductors = old.assigned_conductors | new.assigned_conductors
    else:
        conductors = participation.study.conductors

    recipients = [u.email for u in conductors if u != user]
    if not recipients:
        return 0

    with MailContext('internal') as ctx:
        return ctx.send_mail(
            settings.CASTELLUM_APPOINTMENT_CHANGE_NOTIFICATION_SUBJECT,
            (
                get_mail_data,
                settings.CASTELLUM_APPOINTMENT_CHANGE_NOTIFICATION_BODY,
                settings.CASTELLUM_APPOINTMENT_CHANGE_NOTIFICATION_BODY_EN,
            ),
            recipients,
            cc=[u.email for u in [user] if u],
        )


def get_external_resource_events(resource, start, end):
    if not resource.url:
        return []
    try:
        events = []
        url = (
            resource.url
            .replace('{start}', quote_plus(start.isoformat()))
            .replace('{end}', quote_plus(end.isoformat()))
        )
        text = cached_request(url, timeout=60)
        for event in icalparser.parse_events(text, start=start, end=end):
            events.append({
                'title': f'[{resource}] {event.summary or ""}',
                'start': event.start,
                'end': event.end,
                'backgroundColor': resource.color,
                'borderColor': resource.color,
                'textColor': contrast_color(resource.color),
            })
        return events
    except Exception as e:
        logger.error(f'Fetching events for resource {resource.pk} failed: {e}')
        return []


def fetch_scheduler_appointments(study, base_url, timeout=0):
    count = 0
    now = timezone.now()

    for session in (
        study.studysession_set
        .exclude(schedule_id=None)
        .filter(schedule_updated_at__lt=now - relativedelta(seconds=timeout))
    ):
        try:
            data = scheduler.get_bulk(session.schedule_id)
        except requests.RequestException as e:
            logger.error(f'Fetching schedule for session {session.pk} failed: {e}')
            continue

        session.schedule_updated_at = now
        session.save()

        for participation in session.study.participation_set.filter(
            status__in=Participation.INVITED_SET
        ):
            pseudonym = get_pseudonym(participation.subject, session.domain)
            start = data.get(pseudonym)

            change = Appointment.change(session, participation, start)
            if change:
                count += 1
                send_appointment_notifications(participation, change, base_url)
    return count
