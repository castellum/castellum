# SPDX-FileCopyrightText: 2018-2025 MPIB <https://www.mpib-berlin.mpg.de/>
# SPDX-FileCopyrightText: 2018-2019 MPI-CBS <https://www.cbs.mpg.de/>
# SPDX-FileCopyrightText: 2018-2019 MPIP <http://www.psych.mpg.de/>
# SPDX-License-Identifier: AGPL-3.0-or-later

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext_noop
from django.views.generic import CreateView
from django.views.generic import UpdateView

from castellum.audit.helpers import audit
from castellum.castellum_auth.mixins import PermissionRequiredMixin
from castellum.utils.mail import MailContext

from .forms import AdditionalInfoForm
from .forms import ConsentChangeForm
from .forms import DataProtectionForm
from .models import Consent
from .models import Report
from .models import Subject


class SubjectMixin:
    """Use this on every view that represents a subject.

    Requires ``AccessMixin``.

    -   set ``self.subject``
    -   check privacy level
    """

    def get_subject(self):
        obj = self.get_object()
        if isinstance(obj, Subject):
            return obj
        else:
            return obj.subject

    def check_auth_conditions(self):
        if not super().check_auth_conditions():
            return False
        self.subject = self.get_subject()
        return self.request.user.has_privacy_level(self.subject.privacy_level)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subject'] = self.subject
        return context


class BaseDataProtectionUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subject
    form_class = DataProtectionForm
    permission_required = 'subjects.change_subject'
    template_name = 'subjects/subject_data_protection_form.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        if settings.CASTELLUM_GDPR_NOTIFICATION_TO:
            old = Subject.objects.get(pk=self.object.pk)

            with MailContext('internal') as ctx:
                if self.object.export_requested and not old.export_requested:
                    rel = reverse('subjects:detail', args=[self.object.pk])
                    url = self.request.build_absolute_uri(rel)

                    self.object.to_be_deleted_notified = ctx.send_mail(
                        settings.CASTELLUM_EXPORT_SUBJECT_NOTIFICATION_SUBJECT,
                        (
                            lambda: {'url': url},
                            settings.CASTELLUM_EXPORT_SUBJECT_NOTIFICATION_BODY,
                            settings.CASTELLUM_EXPORT_SUBJECT_NOTIFICATION_BODY_EN,
                        ),
                        settings.CASTELLUM_GDPR_NOTIFICATION_TO,
                    )

                if self.object.to_be_deleted and not self.object.to_be_deleted_notified:
                    rel = reverse('subjects:detail', args=[self.object.pk])
                    url = self.request.build_absolute_uri(rel)

                    self.object.to_be_deleted_notified = ctx.send_mail(
                        settings.CASTELLUM_DELETE_SUBJECT_NOTIFICATION_SUBJECT,
                        (
                            lambda: {'url': url},
                            settings.CASTELLUM_DELETE_SUBJECT_NOTIFICATION_BODY,
                            settings.CASTELLUM_DELETE_SUBJECT_NOTIFICATION_BODY_EN,
                        ),
                        settings.CASTELLUM_GDPR_NOTIFICATION_TO,
                    )

            # If a user explicitly removes the ``to_be_deleted`` they
            # expect that this task is done. If there are still reasons
            # for deletion there should be a new notificaition.
            if not self.object.to_be_deleted and self.object.to_be_deleted_notified:
                old = Subject.objects.get(pk=self.object.pk)
                if old.to_be_deleted:
                    self.object.to_be_deleted_notified = False

        audit(
            gettext_noop('subject updated'),
            self.request.user.username,
            subject_pk=self.object.pk,
        )

        return super().form_valid(form)


class BaseConsentChangeView(PermissionRequiredMixin, CreateView):
    model = Consent
    permission_required = 'subjects.change_subject'
    template_name = 'subjects/subject_consent_form.html'
    form_class = ConsentChangeForm

    def get_object(self):
        # weird because model = Consent, but this is only used in
        # SubjectMixin, not in CreateView
        return get_object_or_404(Subject, pk=self.kwargs['pk'], to_be_deleted=None)

    def form_valid(self, form):
        Consent.objects.filter(subject=self.subject).delete()
        form.instance.subject = self.subject
        form.instance.underage_when_given = self.subject.contact.is_underage
        audit(
            gettext_noop('consent changed'),
            self.request.user.username,
            subject_pk=self.subject.pk,
        )
        return super().form_valid(form)


class BaseAdditionalInfoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Subject
    form_class = AdditionalInfoForm
    permission_required = 'subjects.change_subject'
    template_name = 'subjects/subject_additional_info_form.html'

    def form_valid(self, form):
        audit(
            gettext_noop('subject updated'),
            self.request.user.username,
            subject_pk=self.subject.pk,
        )
        return super().form_valid(form)


class BaseReportView(PermissionRequiredMixin, CreateView):
    model = Report
    fields = ['additional_wishes']
    template_name = 'subjects/subject_report.html'

    def form_valid(self, form):
        if self.subject.blocked:
            raise SuspiciousOperation

        form.instance.subject = self.subject
        form.instance.user = self.request.user
        obj = form.save()

        self.subject.blocked = True
        self.subject.save()

        path = reverse('subjects:report-detail', args=[obj.pk])
        url = self.request.build_absolute_uri(path)

        with MailContext('internal') as ctx:
            ctx.send_mail(
                settings.CASTELLUM_REPORT_NOTIFICATION_SUBJECT,
                (
                    lambda: {'url': url},
                    settings.CASTELLUM_REPORT_NOTIFICATION_BODY,
                    settings.CASTELLUM_REPORT_NOTIFICATION_BODY_EN,
                ),
                settings.CASTELLUM_REPORT_NOTIFICATION_TO,
            )

        messages.success(self.request, _('Your report has been received.'))
        return redirect(self.get_success_url())
